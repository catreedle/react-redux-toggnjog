import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import NavBar from "./common/NavBar"
import Foot from "./common/Foot"
import About from "./components/About";
import HomePage from "./components/HomePage";
import Contact from "./components/Contact";
import Feedback from "./components/Feedback";
import Portfolio from "./components/Portfolio";
import Show from "./components/Show";
import NotFound from "./components/NotFound";
import 'bootstrap/dist/css/bootstrap.min.css';
import './style/style.css'

function App() {
  return (
    <div>
      <NavBar />
      <div style={{ height: "auto" }} fluid>
        <h1 className="display-3 d-flex justify-content-center">{`Togg & Jog`}</h1>
      </div>
      <Router>
        <Routes>
          <Route exact path="/" element={<HomePage />} />
          <Route path="/about" element={<About />} />
          <Route path="/portfolio" element={<Portfolio />} />
          <Route path="/feedback" element={<Feedback />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/show" element={<Show />} />
          <Route element={<NotFound />} />
        </Routes>
      </Router>
      <Foot />
    </div>
  );
}

export default App;
