import React from "react";
import { Col, Row } from "reactstrap"

const Portfolio = () => {
    return (
        <div>
            <div style={{ height: "auto" }} fluid>
                <p className="lead d-flex justify-content-center">
                    here are some of our best work
                </p>
            </div>
            <div className="d-flex justify-content-center">
                <Row className="row-gap">
                    <Col className="col-gap-1">
                        <Row className="row-gap">
                            <img
                                src="https://source.unsplash.com/462x693/?luxury"
                                alt="pic"
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?phone'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?accessory'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x694/?wedding'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x694/?laptop'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?nails'
                                alt='pic'
                            />
                        </Row>

                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x694/?animals'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?fairy'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src="https://source.unsplash.com/462x693/?kids"
                                alt="pic"
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src="https://source.unsplash.com/462x693/?dance"
                                alt="pic"
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?indonesia'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?watch'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?beverage'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src="https://source.unsplash.com/462x693/?festival"
                                alt="pic"
                            />
                        </Row>

                    </Col>
                    <Col className="col-gap-2">
                        <Row className="row-gap">
                            <img
                                src="https://source.unsplash.com/462x693/?citylights"
                                alt="pic"
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?flowers'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x694/?woman'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?dessert'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x694/?lemon'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?temple'
                                alt='pic'
                            />
                        </Row>

                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x694/?bride'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?japan'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src="https://source.unsplash.com/462x693/?sunset"
                                alt="pic"
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src="https://source.unsplash.com/462x693/?chandelier"
                                alt="pic"
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src="https://source.unsplash.com/462x693/?handsome"
                                alt="pic"
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?birds'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?entertainment'
                                alt='pic'
                            />
                        </Row>
                        <Row className="row-gap">
                            <img
                                src='https://source.unsplash.com/462x308/?foodporn'
                                alt='pic'
                            />
                        </Row>
                    </Col>
                </Row>
            </div>
        </div>
    )
}

export default Portfolio