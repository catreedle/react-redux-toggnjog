import React from "react";
import { Col, Row, ListGroup, ListGroupItem } from "reactstrap";

const About = () => {
  return (
    <div className="about-container">
      <div style={{ height: "auto" }} fluid>
        <Row>
          <Col md="6">
            <ListGroup>
              <ListGroupItem>
            <div className="lead d-flex justify-content-center">
              <img
                src={require("../images/tognjog.jpeg")}
                alt="toggnjog"
                width="100%"
              />
            </div>
            </ListGroupItem>
            <ListGroupItem>
              <p className="d-flex justify-content-center">Tyler Joseph and Joshua Dun</p>
            </ListGroupItem>
            </ListGroup>
          </Col>
          <Col md="6">
            <p className="lead d-flex justify-content-center">
              contact my company with any serious photo editing inquiries. your
              photo will be edited, guaranteed or your money back. largest
              amount of money offered for an edit will be put to the front of
              the line, guaranteed. is it your birthday? let us know! birthday
              discount guarantee available for a limited time only. senior (high
              school, college, citizen) discount available upon request.
            </p>
            <Row>
              <Col>
                <img
                  src={require("../images/toggnjogm.jpeg")}
                  alt="toggnjog"
                  width="100%"
                />
              </Col>
              <Col>
                <img
                  src={require("../images/tognjogg2.jpeg")}
                  alt="toggnjog"
                  width="100%"
                />
                <p className="lead d-flex justify-content-center">
                  we are two happy boys from Ohio. when we are not editing
                  photos, we love to take a breathe of money.. i mean fresh air
                </p>
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default About;
