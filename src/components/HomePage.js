import React from "react";
import { Col, Row } from "reactstrap";
import CarouselSection from "./HomePage/Carousel";
import Portfolio from "./Portfolio"

const HomePage = () => {
  return (
    <div>
      <div style={{ height: "auto" }} fluid>
        <p className="lead d-flex justify-content-center">
          local photo editing business company based in Columbus, Ohio. run
          by the two best men in town
        </p>
      </div>
      <Row >
        <Col className="display-3 d-flex justify-content-center" md="3">
          <img
            src="https://mymodernmet.com/wp/wp-content/uploads/2018/07/photo-manipulation-photography-lilia-alvarado-13.jpg"
            alt="girl"
            width="250px"
          />
        </Col>
        <Col md="6">
          <CarouselSection
            className="carousel-section"
            style={{ height: "80%" }}
          />
        </Col>
        <Col md="3">
          <Row className="display-3 d-flex justify-content-center" >
            <img
              src={require("../images/before.jpg")}
              alt="ring1"
              width="250px"
            />
          </Row>
          <Row className="display-3 d-flex justify-content-center" >
            <img
            src={require("../images/after.jpg")}
              alt="ring2"
              width="250px"
            />
          </Row>
        </Col>
      </Row>
      <br></br>
      <br></br>
      <Portfolio />
    </div>
  );
};

export default HomePage;
