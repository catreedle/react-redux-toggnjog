import React from "react";
import { ListGroup, ListGroupItem } from "reactstrap";
import { SocialIcon } from "react-social-icons"

const Contact = () => {
  return (
    <div>
      <div style={{ height: "auto" }} fluid>
        <p className="lead d-flex justify-content-center">
          you can try to contact us at our social medias. please be patient because we are very busy businessmen.</p>
        <ListGroup className="lg-spacing">
          <ListGroupItem>
            <p className="lead d-flex justify-content-center">Togg</p>
          </ListGroupItem>
          <ListGroupItem className="bg-light">
          <div className="lead d-flex justify-content-center">
            <SocialIcon className="social" url="http://twitter.com/tylerrjoseph" />
            <SocialIcon className="social" url="http://instagram.com/tylerrjoseph" />
            <SocialIcon className="social" url="http://facebook.com/tylerrjoseph" />
          </div>
          </ListGroupItem>
        </ListGroup>
        <ListGroup>
          <ListGroupItem>
            <p className="lead d-flex justify-content-center">Jog</p>
          </ListGroupItem>
          <ListGroupItem className="bg-light">
          <div className="lead d-flex justify-content-center">
            <SocialIcon className="social" url="http://twitter.com/joshuadun" />
            <SocialIcon className="social" url="http://instagram.com/joshuadun" />
            <SocialIcon className="social" url="http://facebook.com/joshuadun" />
          </div>
          </ListGroupItem>
        </ListGroup>
      </div>
      </div>
      );
    };
    
    export default Contact;
