import React from "react";
import { Navigate } from "react-router-dom";
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

export default class Feedback extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            email: '',
            message: '',
            redirect: false
        }
        this.AddFeedback = this.AddFeedback.bind(this)
        this.onChangeName = this.onChangeName.bind(this)
        this.onChangeEmail = this.onChangeEmail.bind(this)
        this.onChangeMessage = this.onChangeMessage.bind(this)
    }
    onChangeName(event) {
        this.setState({
            name: event.target.value
        })
    }
    onChangeEmail(event) {
        this.setState({
            email: event.target.value
        })
    }
    onChangeMessage(event) {
        this.setState({
            message: event.target.value
        })
    }
    AddFeedback() {
        fetch("http://reduxblog.herokuapp.com/api/posts?key=toggnjog",
            {
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json',
                },
                redirect: 'follow',
                referrer: 'no-referrer',
                body: JSON.stringify({
                    title: this.state.name,
                    categories: this.state.email,
                    content: this.state.message
                })
            })
            .then(() => {
                this.setState({
                    redirect: true
                })
            })
            
    }
    render() {
        if(this.state.redirect){
            return(
                <Navigate to="/show"></Navigate>
            )
        }
        return (
            <div>
            <div style={{ height: "auto" }} fluid>
                <p className="lead d-flex justify-content-center">
                    give us some feedback</p>
            </div>
            <Form className="form-container">
                <FormGroup>
                    <Label>your name</Label>
                    <Input onChange={this.onChangeName} type="text" name="name" id="name" />
                </FormGroup>
                <FormGroup>
                    <Label>email address</Label>
                    <Input onChange={this.onChangeEmail} type="email" name="email" id="exampleEmail" />
                </FormGroup>
                <FormGroup>
                    <Label>your message</Label>
                    <Input onChange={this.onChangeMessage} type="textarea" name="message" id="message" />
                </FormGroup>
                <Button onClick={this.AddFeedback}>Submit</Button>
            </Form>
        </div>    
        )
    }
}