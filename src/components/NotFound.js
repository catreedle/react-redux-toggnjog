import React from "react"

const NotFound = () => {
    return (
        <div>
            <h1>404</h1>
            What you're looking for is not here. Please check the url.
        </div>
    )

}

export default NotFound