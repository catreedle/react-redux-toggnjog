import React from "react"

const Foot = () => (
    <div className="bg-light ft-spacing">
        <p className=" d-flex justify-content-center">©catreedle 2019</p>
        <p className=" d-flex justify-content-center">for inquiries, email me at purnamasrahayu@gmail.com</p>
    </div>
)

export default Foot