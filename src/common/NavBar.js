import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';

export default class NavBar extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <Navbar sticky="top" color="light" light expand="md">
        <Collapse className="d-flex justify-content-center" isOpen={this.state.isOpen} navbar>
          <Nav navbar>
            <NavbarBrand className
              ="navbar-brand ml-auto d-none d-lg-block" href="/">TNJ</NavbarBrand>

            <NavItem>
              <NavLink href="/about">about</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/portfolio">portfolio</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/feedback">feedback</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="/contact">contact us</NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>

    );
  }
}